﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjectGame.RudeRoot.Player
{
    public class PlayerActionAttack : PlayerAction
    {
        protected override void Start()
        {
            preset = GetComponent<PlayerController>()._Presets;
            MaxDowntimeDuration = preset.status.AttackSpeed == 0 ? Mathf.Infinity : 1f / preset.status.AttackSpeed;
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void Action()
        {
            base.Action();
        }

        public void Action(PlayerMoveHorizontal horiAction)
        {
            Action();
            if (horiAction.InputOnCamera.y <= 0)
                transform.rotation = Quaternion.LookRotation(horiAction.InputOnCamera);
            horiAction.AddForceContinuously(new Vector2(transform.forward.x, transform.forward.z) * 20, Time.fixedDeltaTime * 3f);
        }
    }
}