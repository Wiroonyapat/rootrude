using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGame.RudeRoot.Player
{

    [CreateAssetMenu(fileName = "PlayerPreset", menuName = "Player/PlayerPreset")]
    public class PlayerActionPresets : ScriptableObject
    {
        public Move moveVar = new(10f, 2000f);
        public Jump jumpVar = new(2f, 0.15f, 0.2f);
        public AirFlow flowVar = new(0.2f, 19.62f, 2f);
        public Status status = new(1f, 1f, 1.3f, 5f);
        public Ability actionsList = new();
    }

    [Serializable]
    public struct Move
    {

        public Move(float speed, float angular)
        {
            Speed = speed;
            AngularSpeed = angular;
        }
        [Tooltip("A Speed that Player can move in 1 second")]
        [Range(0f, 30f)] public float Speed;
        [Tooltip("A Speed that Player can turn self to correct direction")]
        [Range(0f, 5000f)] public float AngularSpeed;
    }

    [Serializable]
    public struct Jump
    {

        public Jump(float height, float scale, float timelimit)
        {
            JumpHeight = height;
            JumpDynamicScale = scale;
            JumpDynamicTimeLimit = timelimit;
        }

        [Tooltip("A Height that Player can Jump")]
        [Range(0.01f, 10f)] public float JumpHeight;
        [Tooltip("A Height Scale between (Press then Release) and (Hold then Release)")]
        [Range(0.01f, 1f)] public float JumpDynamicScale;
        [Tooltip("A Maximum time of holding Jump")]
        [Range(0f, 1f)] public float JumpDynamicTimeLimit;
    }

    [Serializable]
    public struct AirFlow
    {

        public AirFlow(float timelimit, float gforce, float scale)
        {
            CoyoteTimeLimit = timelimit;
            GravityForce = gforce;
            AirborneScale = scale;
        }

        [Tooltip("A Time that Player can still press Jump while falling and not Jump yet")]
        [Range(0f, 2f)] public float CoyoteTimeLimit;
        [Tooltip("A Force that pulling Player to the Ground Continuously")]
        public float GravityForce;
        [Tooltip("A Scale between (Jump up) and (Fall down)")]
        [Range(0f, 10f)] public float AirborneScale;
    }

    [Serializable]
    public struct Status
    {
        public Status(float atk, float ap, float aaTime, float abTime)
        {
            AttackPower = atk;
            AbilityPower = ap;
            AttackSpeed = aaTime;
            AbilityCooldown = abTime;
        }

        [Tooltip("A Power of ATK or Normal Attack")]
        [Range(0, 100f)] public float AttackPower;
        [Tooltip("A Power of Ability or Skill")]
        [Range(0, 100f)] public float AbilityPower;
        [Tooltip("A Speed that Player can Normal Attack in 1 second")]
        [Range(0f, 10f)] public float AttackSpeed;
        [Tooltip("A Cooldown of Player's Ability")]
        [Range(0f, 60f)] public float AbilityCooldown;
    }

    [Serializable]
    public struct Ability
    {
        [Tooltip("Prefab of GameObject that store PlayerAction Component")]
        public PlayerAction AbilityClass;
    }
}
