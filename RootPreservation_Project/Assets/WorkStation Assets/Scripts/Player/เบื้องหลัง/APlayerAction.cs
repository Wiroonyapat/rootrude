using UnityEngine;

namespace ProjectGame.RudeRoot.Player
{

    public abstract class APlayerAction : MonoBehaviour
    {
        // Start is called before the first frame update
        protected virtual void Start()
        {

        }

        // Update is called once per frame
        protected virtual void Update()
        {

        }

        public abstract void Action();

    }
}
