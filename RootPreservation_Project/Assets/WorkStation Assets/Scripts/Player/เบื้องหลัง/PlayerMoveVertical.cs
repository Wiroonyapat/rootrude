using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ProjectGame.RudeRoot.Player
{
    public class PlayerMoveVertical : APlayerAction
    {

        public float Velocity
        {
            get => m_velocity;
        }

        public bool IsJumped
        {
            get => m_isJumping;
        }


        private PlayerActionPresets preset;
        private CharacterController controller;


        private float m_velocity;
        private bool m_isJumping;
        private float m_lastVelocity;
        private bool m_isPerfromJump;
        private bool m_canPerformJump = true;
        private float m_jumpTiming;
        private float m_jumpDistanceAgo;
        private bool m_isCoyoteTime;
        private float m_falldownTime;

        private SphereCollider sphere;


        protected override void Start()
        {
            controller = GetComponent<CharacterController>();
            preset = GetComponent<PlayerController>()._Presets;
            sphere = GetComponent<SphereCollider>();
        }

        protected override void Update()
        {

            float vertiVelo = 0;


            vertiVelo = m_velocity;
            if (!m_isJumping)
            {
                m_canPerformJump = JumpChecker();
            }

                
            if (m_isJumping && controller.isGrounded)
            {
                vertiVelo = AirborneCalculate();
                m_canPerformJump = false;
                m_isCoyoteTime = false;
            }
            else if (m_isPerfromJump && m_jumpTiming < preset.jumpVar.JumpDynamicTimeLimit)
            {
                m_jumpTiming += Time.deltaTime;
                m_jumpTiming = Mathf.Clamp(m_jumpTiming, 0, preset.jumpVar.JumpDynamicTimeLimit);
                vertiVelo = AirborneCalculate();
                m_jumpDistanceAgo +=
                    (vertiVelo - preset.flowVar.GravityForce * (1 / preset.flowVar.AirborneScale) * Time.deltaTime) *
                    Time.deltaTime;
            }
            else if (m_isJumping && !controller.isGrounded && vertiVelo <= 0)
            {
                if (GroundChecker())
                {
                    m_isJumping = false;
                    m_canPerformJump = true;
                }
            }


            if (!controller.isGrounded || (controller.isGrounded && m_isCoyoteTime))
            {
                if (vertiVelo <= 0) vertiVelo -= preset.flowVar.GravityForce * Time.deltaTime;
                if (vertiVelo > 0) vertiVelo -= preset.flowVar.GravityForce * (1f / preset.flowVar.AirborneScale) * Time.deltaTime;
            }

                
            if ((controller.isGrounded || (m_canPerformJump && !m_isCoyoteTime)) && vertiVelo < 0)
            {
                vertiVelo = -2;
            }

            m_velocity = vertiVelo;

            controller.Move(new Vector3(0, m_velocity, 0) * Time.deltaTime);

        }

        public override void Action()
        {

        }

        public void Action(InputAction.CallbackContext context)
        {
            if (context.started && !m_isJumping && (controller.isGrounded || m_isCoyoteTime) && m_canPerformJump)
            {
                m_isJumping = true;
                m_isPerfromJump = true;
                m_canPerformJump = false;
                m_jumpTiming = 0;
                m_jumpDistanceAgo = 0;
            }

            if (context.canceled)
            {
                m_isPerfromJump = false;
            }
        }


        private bool JumpChecker()
        {

            int ground = LayerMask.NameToLayer("Ground");

            Collider[] sphereCast =
                Physics.OverlapSphere(transform.position + sphere.center, sphere.radius, 1 << ground);

            if (controller.isGrounded || sphereCast.Length > 0)
            {
                m_falldownTime = preset.flowVar.CoyoteTimeLimit;
                m_isCoyoteTime = false;
                return true;
            }
            else if (m_falldownTime > 0)
            {
                m_falldownTime -= Time.deltaTime;
                m_isCoyoteTime = true;
                return true;
            }
            else if (m_isCoyoteTime)
            {
                m_isCoyoteTime = false;
                return false;
            }

            return false;
        }

        private bool GroundChecker()
        {

            int ground = LayerMask.NameToLayer("Ground");

            Collider[] sphereCast =
                Physics.OverlapSphere(transform.position + sphere.center, sphere.radius, 1 << ground);

            m_isCoyoteTime = false;

            if (controller.isGrounded || sphereCast.Length > 0)
            {
                return true;
            }

            return false;

        }

        private float AirborneCalculate()
        {
            float minHeight = preset.jumpVar.JumpDynamicScale * preset.jumpVar.JumpHeight;
            float divideResult = preset.jumpVar.JumpDynamicTimeLimit == 0 ? 1 : m_jumpTiming / preset.jumpVar.JumpDynamicTimeLimit;
            float dynamicHeight = Mathf.Lerp(minHeight, preset.jumpVar.JumpHeight, divideResult) - m_jumpDistanceAgo;
            float velocityReturn = Mathf.Sqrt(2f * preset.flowVar.GravityForce * (1f / preset.flowVar.AirborneScale) * dynamicHeight);

            return velocityReturn;
        }


        public void AddForceOneFrame(float YForce)
        {
            if (YForce > 0) ResetVelocity();
            StartCoroutine(AddForce(YForce, Time.deltaTime));
        }

        public void AddForceContinuously(float YForce, float duration)
        {
            if (YForce > 0) ResetVelocity();
            StartCoroutine(AddForce(YForce, duration));
        }

        private IEnumerator AddForce(float force, float time)
        {
            float countdown = 0;
            while (countdown < time)
            {
                controller.Move(new Vector3(0, force * Time.deltaTime, 0));
                countdown += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }

        public void ResetVelocity()
        {
            m_velocity = -2;
        }
    }
}
