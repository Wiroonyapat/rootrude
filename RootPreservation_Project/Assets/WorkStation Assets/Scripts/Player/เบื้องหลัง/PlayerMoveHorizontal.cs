using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGame.RudeRoot.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMoveHorizontal : APlayerAction
    {
        public Vector2 Velocity
        {
            get => m_velocity;
        }
        public Vector3 InputOnCamera
        {
            get
            {
                var cam_Hori = Camera.main.transform.right * m_inputValue.x;
                cam_Hori.y = 0;
                var cam_Verti = Camera.main.transform.up * m_inputValue.y;
                cam_Verti.y = 0;

                if ((cam_Hori + cam_Verti).magnitude == 0) return Vector3.up;
                return (cam_Hori + cam_Verti).normalized;
            }
        }


        private CharacterController controller;
        private PlayerActionPresets preset;

        private float m_speed;
        private float m_angularSpeed;
        private Vector2 m_velocity = new();
        private Vector2 m_inputValue;

        protected override void Start()
        {
            controller = GetComponent<CharacterController>();
            preset = GetComponent<PlayerController>()._Presets;
            m_speed = preset.moveVar.Speed;
            m_angularSpeed = preset.moveVar.AngularSpeed;
        }

        protected override void Update()
        {
            if (m_inputValue.magnitude != 0)
            {

                Quaternion newRotation = Quaternion.LookRotation(InputOnCamera);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation,
                    m_angularSpeed * Time.deltaTime);

                if (Quaternion.Angle(transform.rotation, newRotation) <= 25f)
                {

                    m_velocity.x = m_velocity.x + (transform.forward.x * m_speed - m_velocity.x) * 0.125f;
                    m_velocity.y = m_velocity.y + (transform.forward.z * m_speed - m_velocity.y) * 0.125f;

                    controller.Move(new Vector3(m_velocity.x, 0, m_velocity.y) * Time.deltaTime);
                }
                else
                {
                    m_velocity = Vector2.Lerp(m_velocity, Vector2.zero, 0.125f);
                    controller.Move(new Vector3(m_velocity.x, 0, m_velocity.y) * Time.deltaTime);
                }
            }
            else
            {
                m_velocity = Vector2.Lerp(m_velocity, Vector2.zero, 0.125f);
                controller.Move(new Vector3(m_velocity.x, 0, m_velocity.y) * Time.deltaTime);
            }
        }

        public override void Action()
        {
            //Ignore
        }

        public void Action(Vector2 inputValue)
        {
            m_inputValue = inputValue;
        }

        public void AddForceOneFrame(Vector2 XZForce)
        {
            StartCoroutine(AddForce(XZForce, Time.deltaTime));
        }

        public void AddForceContinuously(Vector2 XZForce, float duration)
        {
            StartCoroutine(AddForce(XZForce, duration));
        }

        private IEnumerator AddForce(Vector2 force, float time)
        {
            float countdown = 0;
            while (countdown < time)
            {
                controller.Move(new Vector3(force.x * Time.deltaTime, 0, force.y * Time.deltaTime));
                countdown += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }

        public void SetSpeed(float setTo)
        {
            m_speed = setTo;
        }

        public void DefaultSpeed()
        {
            m_speed = preset.moveVar.Speed;
        }

        public void SetAngularSpeed(float setTo)
        {
            m_angularSpeed = setTo;
        }

        public void DefaultAngularSpeed()
        {
            m_angularSpeed = preset.moveVar.AngularSpeed;
        }
    }
}