using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGame.RudeRoot.Player
{

    public class PlayerAction : APlayerAction
    {

        public enum ActionState
        {
            Downtime,
            Showtime
        }

        public ActionState _ActionState
        {
            get
            {
                if (DowntimeDuration <= 0) return ActionState.Showtime;
                else return ActionState.Downtime;
            }
        }

        public float MaxDowntimeDuration
        {
            get => m_maxdowntime;
            protected set
            {
                if (value < 0)
                {
                    m_maxdowntime = 0;
                }
                else
                {
                    m_maxdowntime = value;
                }
            }
        }

        public float DowntimeDuration
        {
            get { return m_downtime;}
            protected set => m_downtime = Mathf.Clamp(value, 0, MaxDowntimeDuration);
        }

        protected float m_downtime;
        protected float m_maxdowntime;


        /// <summary>
        /// A Percent of Downtime
        /// </summary>
        /// <returns> 1 - (Downtime / Max)
        /// </returns>
        public float PercentDuration
        {
            get
            {
                if (MaxDowntimeDuration <= 0)
                {
                    return 1;
                }

                return 1 - (DowntimeDuration / MaxDowntimeDuration);
            }
        }


        protected PlayerActionPresets preset;
        

        protected override void Start()
        {
            preset = GetComponent<PlayerController>()._Presets;
        }

        protected override void Update()
        {
            if (_ActionState != ActionState.Showtime)
            {
                DowntimeDuration -= Time.deltaTime;
            }
        }

        public override void Action()
        {
            DowntimeDuration = MaxDowntimeDuration;
        }

        public void AddActionTime(float addTo)
        {
            DowntimeDuration += addTo;
        }

        public void SetActionTime(float setTo)
        {
            DowntimeDuration = setTo;
        }

        public void SetActionUseLongestTime(float setTo)
        {
            if (setTo > DowntimeDuration)
                DowntimeDuration = setTo;
        }

    }
}
