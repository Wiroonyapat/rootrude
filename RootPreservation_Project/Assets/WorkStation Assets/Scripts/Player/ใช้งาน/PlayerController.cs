using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ProjectGame.RudeRoot.Player
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(SphereCollider))]
    public class PlayerController : MonoBehaviour
    {

        public PlayerActionPresets _Presets;

        [HideInInspector]
        public PlayerMoveHorizontal _horizontal;

        [HideInInspector]
        public PlayerMoveVertical _vertical;

        [HideInInspector]
        public PlayerActionAttack _attackaction;

        [SerializeField] private bool FreezeInput = false;
        [SerializeField] private bool FreezeMoveInput = false;
        [SerializeField] private bool FreezeJumpInput = false;
        [SerializeField] private bool FreezeAttackInput = false;

        private Vector2 input;

        public void MoveInput(InputAction.CallbackContext context)
        {

            input = context.ReadValue<Vector2>();

        }

        public void JumpInput(InputAction.CallbackContext context)
        {
            if (!FreezeInput && !FreezeJumpInput)
                _vertical.Action(context);
        }

        public void AttackInput(InputAction.CallbackContext context)
        {
            if (!FreezeInput && !FreezeAttackInput && _attackaction._ActionState == PlayerAction.ActionState.Showtime && context.started)
                _attackaction.Action(_horizontal);
        }


        // Start is called before the first frame update
        void Start()
        {
            _horizontal = gameObject.AddComponent<PlayerMoveHorizontal>();
            _vertical = gameObject.AddComponent<PlayerMoveVertical>();
            _attackaction = gameObject.AddComponent<PlayerActionAttack>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_attackaction._ActionState != PlayerAction.ActionState.Showtime)
            {
                FreezeInput = true;
                _horizontal.Action(Vector2.zero);
            }
            else
            {
                FreezeInput = false;
                _horizontal.Action(input);
            }
        }
    }
}
